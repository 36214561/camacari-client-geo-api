package br.com.geomais.clientecamacari.teste;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.geomais.clientecamacari.api.util.ParserUtil;
import br.com.geomais.clientecamacari.model.ApiResponseMessage;

public class UtililitarioTest {

	public static String converterListaApiResponseMessageToString(InputStream inputStream) throws IOException {
		ParserUtil<ApiResponseMessage[]> parserUtil = new ParserUtil<>(ApiResponseMessage[].class);
		ApiResponseMessage[] bodyObj = parserUtil.toObject(inputStream);
		StringBuilder message = new StringBuilder();
		for (int i = 0; i < bodyObj.length; i++) {
			message.append(bodyObj[i].getCode()).append(" - ").append(bodyObj[i].getMessage()).append(".\\n")
					.append(bodyObj[i].getDescription());
		}
		return message.toString();
	}

	public static String converterApiResponseMessageToString(InputStream inputStream) throws IOException {
		ParserUtil<ApiResponseMessage> parserUtil = new ParserUtil<>(ApiResponseMessage.class);
		ApiResponseMessage bodyObj = parserUtil.toObject(inputStream);
		StringBuilder message = new StringBuilder();
		message.append(bodyObj.getCode()).append(" - ").append(bodyObj.getMessage()).append(".\\n")
				.append(bodyObj.getDescription());
		return message.toString();
	}

	public static Object trataResposta(Response response, Class<?> classeSucesso) throws IOException {
		InputStream inputStream = (InputStream) response.getEntity();
		// Se o status http não for 200 - sucesso
		if (response.getStatus() != Status.OK.getStatusCode()) {
			if (response.getStatus() == Status.BAD_REQUEST.getStatusCode()) {
				String message = UtililitarioTest.converterListaApiResponseMessageToString(inputStream);
				fail(("A informação enviada na requisição foi mal formada.\\n").concat(message));
				return null;
			} else {
				String message = UtililitarioTest.converterApiResponseMessageToString(inputStream);
				fail(("HTTP Status ").concat(String.valueOf(response.getStatus())).concat("\\n").concat(message));
				return null;
			}
		}

		ParserUtil<?> parserUtil = new ParserUtil<>(classeSucesso);
		return parserUtil.toObject(inputStream);
	}
}
