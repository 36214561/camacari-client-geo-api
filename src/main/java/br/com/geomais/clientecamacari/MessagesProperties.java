package br.com.geomais.clientecamacari;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Classe que simplifica a obtenção de um valor de uma propriedade do
 * messages.properties
 * 
 * @author jordani
 */
public class MessagesProperties {
	private static Locale ptBr = new Locale("pt", "BR");
	private static ResourceBundle bundle = ResourceBundle.getBundle("messages", ptBr);

	/**
	 * Obtem o valor atraves da chave
	 * 
	 * @param key
	 *            chave
	 */
	public static String getMessage(String key) {
		return bundle.getString(key);
	}

	/**
	 * Obtem o valor atraves da chave
	 * 
	 * @param key
	 *            chave
	 * @param args
	 *            argumentos
	 */
	public static String getMessage(String key, Object... args) {
		return MessageFormat.format(bundle.getString(key), args);
	}

}