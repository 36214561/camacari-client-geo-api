package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.BairroApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwBairro;

/**
 * Classe de acesso as operações do recurso Bairro
 * 
 * @author jordani
 * 
 * 
 */
public class BairroClient extends ConsultaClient<CollectionVwBairro> {

	public BairroClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return BairroApi.class;
	}

}
