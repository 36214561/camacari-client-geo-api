package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.ImovelApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwImovel;
import br.com.geomais.clientecamacari.model.VwImovel;

public class ImovelClient extends ConsultaClient<CollectionVwImovel> {

	public ImovelClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return ImovelApi.class;
	}
}
