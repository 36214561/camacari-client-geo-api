package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.CadastroLogradouroApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwCadastroLogradouro;
import br.com.geomais.clientecamacari.model.VwCadastroLogradouro;

public class CadastroLogradouroClient extends ConsultaClient<CollectionVwCadastroLogradouro> {

	public CadastroLogradouroClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return CadastroLogradouroApi.class;
	}
}
