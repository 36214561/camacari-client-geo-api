package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.LoteamentoApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwLoteamento;
import br.com.geomais.clientecamacari.model.VwLoteamento;

public class LoteamentoClient extends ConsultaClient<CollectionVwLoteamento> {

	public LoteamentoClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return LoteamentoApi.class;
	}
}
