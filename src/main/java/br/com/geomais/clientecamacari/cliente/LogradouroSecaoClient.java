package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.LogradouroSecaoApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwLogradouroSecao;
import br.com.geomais.clientecamacari.model.VwLogradouroSecao;

public class LogradouroSecaoClient extends ConsultaClient<CollectionVwLogradouroSecao> {

	public LogradouroSecaoClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return LogradouroSecaoApi.class;
	}
}
