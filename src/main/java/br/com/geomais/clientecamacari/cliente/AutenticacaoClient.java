package br.com.geomais.clientecamacari.cliente;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import br.com.geomais.clientecamacari.Const;
import br.com.geomais.clientecamacari.MessagesProperties;
import br.com.geomais.clientecamacari.api.AutenticacaoApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.ApiResponseMessage;

/**
 * Classe de acesso aos recursos de autenticação
 * 
 * @author jordani
 * 
 * 
 */
public class AutenticacaoClient extends Client {

	private AutenticacaoApi api;

	public AutenticacaoClient() {
		JacksonJsonProvider provider = new JacksonJsonProvider();
		List providers = new ArrayList();
		providers.add(provider);

		api = JAXRSClientFactory.create(Const.URL, AutenticacaoApi.class, providers);
		org.apache.cxf.jaxrs.client.Client client = WebClient.client(api);

		ClientConfiguration config = WebClient.getConfig(client);
	}

	public String gerarToken(String usuario, String senha) throws SistemaException, AplicacaoException {
		try {
			String credencialCodificada = Base64Utility.encode(usuario.concat(":").concat(senha).getBytes());
			String authorization = "Basic ".concat(credencialCodificada);
			Response response = api.generateToken(authorization);
			ApiResponseMessage resposta = (ApiResponseMessage) trataResposta(response, ApiResponseMessage.class);

			String token = response.getMetadata().getFirst(HttpHeaders.AUTHORIZATION).toString();
			String dateEE_MMM_DD = response.getMetadata().getFirst("x-expires-after").toString();
			Date date = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US).parse(dateEE_MMM_DD);
			return token;
		} catch (Exception e) {
			throw new SistemaException(MessagesProperties.getMessage("error.gerartoken"), e);
		}
	}
}
