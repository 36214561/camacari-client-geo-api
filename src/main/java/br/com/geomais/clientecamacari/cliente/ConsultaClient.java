package br.com.geomais.clientecamacari.cliente;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import br.com.geomais.clientecamacari.Const;
import br.com.geomais.clientecamacari.api.QueryApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;

/**
 * Implementação generica de serviços de consulta
 * 
 * @author jordani
 * 
 */
public abstract class ConsultaClient<T> extends Client {

	private String token;
	private QueryApi api;
	private Class entidadeClass;
	private String usuario;
	private String senha;

	public ConsultaClient(String usuario, String senha) throws SistemaException, AplicacaoException {

		this.usuario = usuario;
		this.senha = senha;
		this.entidadeClass = obterTipoEntidade();

		JacksonJsonProvider provider = new JacksonJsonProvider();
		List providers = new ArrayList();
		providers.add(provider);

		api = JAXRSClientFactory.create(Const.URL, getApiClass(), providers);
		org.apache.cxf.jaxrs.client.Client client = WebClient.client(api);

		ClientConfiguration config = WebClient.getConfig(client);
		gerenciarToken();
	}

	protected abstract Class<? extends QueryApi> getApiClass();

	public String getToken() {
		return token;
	}

	private void gerenciarToken() throws SistemaException, AplicacaoException {
		boolean tokenExpirado = true;
		if (tokenExpirado) {
			AutenticacaoClient autenticacaoClient = new AutenticacaoClient();
			token = autenticacaoClient.gerarToken(usuario, senha);
		}
	}

	private Class obterTipoEntidade() {
		return ((Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
	}

	public T obterRecursos(String params, Integer skip, Integer limit) throws SistemaException, AplicacaoException {
		Response response = api.executeGet(params, skip, limit, getToken());
		return (T) trataResposta(response, obterTipoEntidade());
	}
}
