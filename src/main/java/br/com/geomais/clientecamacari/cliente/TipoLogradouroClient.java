package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.TipoLogradouroApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwTipoLogradouro;
import br.com.geomais.clientecamacari.model.VwTipoLogradouro;

public class TipoLogradouroClient extends ConsultaClient<CollectionVwTipoLogradouro> {

	public TipoLogradouroClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return TipoLogradouroApi.class;
	}
}
