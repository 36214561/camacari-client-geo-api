package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.LogradouroApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwLogradouro;
import br.com.geomais.clientecamacari.model.VwLogradouro;

public class LogradouroClient extends ConsultaClient<CollectionVwLogradouro> {

	public LogradouroClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return LogradouroApi.class;
	}
}
