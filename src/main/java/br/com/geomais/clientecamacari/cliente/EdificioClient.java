package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.EdificioApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwEdificio;
import br.com.geomais.clientecamacari.model.VwEdificio;

public class EdificioClient extends ConsultaClient<CollectionVwEdificio> {

	public EdificioClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return EdificioApi.class;
	}
}
