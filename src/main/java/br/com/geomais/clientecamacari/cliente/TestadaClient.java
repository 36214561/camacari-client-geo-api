package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.TestadaApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwTestada;
import br.com.geomais.clientecamacari.model.VwTestada;

public class TestadaClient extends ConsultaClient<CollectionVwTestada> {

	public TestadaClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return TestadaApi.class;
	}
}
