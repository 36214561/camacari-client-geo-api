package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.PessoaApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwPessoa;

public class PessoaClient extends ConsultaClient<CollectionVwPessoa> {

	public PessoaClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return PessoaApi.class;
	}
}
