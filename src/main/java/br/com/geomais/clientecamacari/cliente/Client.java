package br.com.geomais.clientecamacari.cliente;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.geomais.clientecamacari.MessagesProperties;
import br.com.geomais.clientecamacari.api.util.ParserUtil;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.DescricaoValue;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.ApiResponseMessage;

/**
 * Classe abstrata
 * 
 *
 */
public abstract class Client {

	protected Object trataResposta(Response response, Class<?> classeSucesso)
			throws SistemaException, AplicacaoException {
		InputStream inputStream = (InputStream) response.getEntity();
		// Se o status http não for 200 - sucesso
		if (response.getStatus() != Status.OK.getStatusCode()) {
			if (response.getStatus() == Status.BAD_REQUEST.getStatusCode()) {
				converterListaDeErros(inputStream);
			} else {
				converterErro(inputStream);
			}
		}

		ParserUtil<Object> parserUtil = new ParserUtil<>(classeSucesso);
		try {
			return parserUtil.toObject(inputStream);
		} catch (IOException e) {
			throw new SistemaException(MessagesProperties.getMessage("error.serializedObject"), e);
		}
	}

	private void converterParaExcecao(ApiResponseMessage[] bodyObj) throws AplicacaoException {
		AplicacaoException aplicacaoException = new AplicacaoException("Ocorreram problemas na aplicação");
		Collection<DescricaoValue> descricaoErros = formatarErro(bodyObj);
		for (DescricaoValue descricaoValue : descricaoErros) {
			aplicacaoException.addErro(descricaoValue);
		}
		throw aplicacaoException;
	}

	private void converterListaDeErros(InputStream inputStream) throws SistemaException, AplicacaoException {
		ParserUtil<ApiResponseMessage[]> parserUtil = new ParserUtil<>(ApiResponseMessage[].class);
		ApiResponseMessage[] bodyObj;
		try {
			bodyObj = parserUtil.toObject(inputStream);
		} catch (IOException e) {
			throw new SistemaException(MessagesProperties.getMessage("error.serializedObject"), e);
		}
		converterParaExcecao(bodyObj);
	}

	private void converterErro(InputStream inputStream) throws SistemaException, AplicacaoException {
		ParserUtil<ApiResponseMessage> parserUtil = new ParserUtil<>(ApiResponseMessage.class);
		ApiResponseMessage bodyObj;
		try {
			bodyObj = parserUtil.toObject(inputStream);
		} catch (IOException e) {
			throw new SistemaException(MessagesProperties.getMessage("error.serializedObject"), e);
		}
		converterParaExcecao(new ApiResponseMessage[] { bodyObj });
	}

	private Collection<DescricaoValue> formatarErro(ApiResponseMessage[] bodyObj) {

		Collection<DescricaoValue> colDescricao = new ArrayList<DescricaoValue>();

		StringBuilder mensagem = new StringBuilder();
		StringBuilder detalhe = new StringBuilder();
		for (int i = 0; i < bodyObj.length; i++) {
			mensagem.append("Código:").append(bodyObj[i].getCode()).append(" - ").append(bodyObj[i].getMessage());
			detalhe.append(bodyObj[i].getDescription());
			DescricaoValue descricaoValue = new DescricaoValue(mensagem.toString(), detalhe.toString());
			colDescricao.add(descricaoValue);
		}
		return colDescricao;
	}

}
