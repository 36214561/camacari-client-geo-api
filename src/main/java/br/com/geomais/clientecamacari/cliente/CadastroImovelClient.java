package br.com.geomais.clientecamacari.cliente;

import br.com.geomais.clientecamacari.api.CadastroImovelApi;
import br.com.geomais.clientecamacari.excecoes.AplicacaoException;
import br.com.geomais.clientecamacari.excecoes.SistemaException;
import br.com.geomais.clientecamacari.model.CollectionVwCadastroImovel;
import br.com.geomais.clientecamacari.model.VwCadastroImovel;

public class CadastroImovelClient extends ConsultaClient<CollectionVwCadastroImovel> {

	public CadastroImovelClient(String usuario, String senha) throws SistemaException, AplicacaoException {
		super(usuario, senha);
	}

	@Override
	protected Class getApiClass() {
		return CadastroImovelApi.class;
	}
}
