package br.com.geomais.clientecamacari;

/**
 * Constante
 * 
 * @author jordani
 * 
 */
public class Const {
	/** https://10.0.9.140:8080/CamacariGeoApi/v1 */
	public static final String URL = MessagesProperties.getMessage("config.protocol").concat("://")
			.concat(MessagesProperties.getMessage("config.host"))
			.concat(MessagesProperties.getMessage("config.baseUrl"));

	/** Informação adicional */
	public static final int INFO = 1;
	/** Execução ocorreu com sucesso */
	public static final int OK = 2;
	/**
	 * Mensagem de aviso ou advertência, mas que não interfere no processamento da
	 * mensagem
	 */
	public static final int WARNING = 3;
	/** Falha ou problema que afeta a execução */
	public static final int ERROR = 4;
	/** Proposta, conselho ou dica */
	public static final int SUGGESTION = 5;

	public static final String BEARER = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZSIsIngiOiJFMTBBREMzOTQ5QkE1OUFCQkU1NkUwNTdGMjBGODgzRSIsImV4cCI6MTUwODE2NzQ5Nn0.jEx8skGCBJZNn505ts9nRwcLTHghPZG49tV7wNg7QbqTCBOmspKhvUQsGNYMQuUWhATMAdDsc9oZnDdPQSGIfA";
}
