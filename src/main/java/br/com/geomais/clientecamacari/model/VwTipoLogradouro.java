package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwTipoLogradouro {

	private Integer cdTipoLogradouro = null;

	private String dsTipoLogradouro = null;

	private String dsTipoLogradouroAbreviado = null;

	@XmlElement(name = "cd_tipo_logradouro")
	@JsonProperty(value = "cd_tipo_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Tipo de Logradouro")
	public Integer getCdTipoLogradouro() {
		return this.cdTipoLogradouro;
	}

	public void setCdTipoLogradouro(Integer cdTipoLogradouro) {
		this.cdTipoLogradouro = cdTipoLogradouro;
	}

	@XmlElement(name = "ds_tipo_logradouro")
	@JsonProperty(value = "ds_tipo_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Descrição do Tipo de Logradouro")
	public String getDsTipoLogradouro() {
		return this.dsTipoLogradouro;
	}

	public void setDsTipoLogradouro(String dsTipoLogradouro) {
		this.dsTipoLogradouro = dsTipoLogradouro;
	}

	@XmlElement(name = "ds_tipo_logradouro_abreviado")
	@JsonProperty(value = "ds_tipo_logradouro_abreviado", defaultValue = "")
	@ApiModelProperty(value = "Descrição Abreviada do Tipo de Logradouro")
	public String getDsTipoLogradouroAbreviado() {
		return this.dsTipoLogradouroAbreviado;
	}

	public void setDsTipoLogradouroAbreviado(String dsTipoLogradouroAbreviado) {
		this.dsTipoLogradouroAbreviado = dsTipoLogradouroAbreviado;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwTipoLogradouro {\n");

		sb.append("    cdTipoLogradouro: ").append(toIndentedString(cdTipoLogradouro)).append("\n");
		sb.append("    dsTipoLogradouro: ").append(toIndentedString(dsTipoLogradouro)).append("\n");
		sb.append("    dsTipoLogradouroAbreviado: ").append(toIndentedString(dsTipoLogradouroAbreviado)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
