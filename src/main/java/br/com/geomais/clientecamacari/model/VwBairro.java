package br.com.geomais.clientecamacari.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class VwBairro {

	private String cdBairro = null;
	private String nmBairro = null;

	@XmlElement(name = "cd_bairro")
	@JsonProperty(value = "cd_bairro", defaultValue = "")
	@ApiModelProperty(value = "Código do bairro")
	public String getCdBairro() {
		return this.cdBairro;
	}

	public void setCdBairro(String cdBairro) {
		this.cdBairro = cdBairro;
	}

	@XmlElement(name = "nm_bairro")
	@JsonProperty(value = "nm_bairro", defaultValue = "")
	@ApiModelProperty(value = "Nome do bairro")
	public String getNmBairro() {
		return this.nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

}
