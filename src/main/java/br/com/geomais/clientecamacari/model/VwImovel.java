package br.com.geomais.clientecamacari.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwImovel {

	private BigDecimal areaCobertComum = null;

	private BigDecimal areaCoberta = null;

	private BigDecimal areaComPreserv = null;

	private BigDecimal areaComPreservComum = null;

	private BigDecimal areaComPreservVinc = null;

	private BigDecimal areaDescobertComum = null;

	private BigDecimal areaDescoberta = null;

	private BigDecimal areaDescobertaSecundaria = null;

	private BigDecimal areaMezanino = null;

	private BigDecimal areaMezaninoComum = null;

	private BigDecimal areaSemPreserv = null;

	private BigDecimal areaSemPreservComum = null;

	private BigDecimal areaSemPreservVinc = null;

	private String bairroLocal = null;

	private String cdDistrito = null;

	private String cdLoteamento = null;

	private String cdSetor = null;

	private String codPropPrinc = null;

	private String codTipoContribuinte = null;

	private String codTipoImovel = null;

	private String codigoLogradouroLocal = null;

	private String complementoLocal = null;

	private String convenioAgua = null;

	private String convenioEnergia = null;

	private Integer idPessoa = null;

	private String loteGeo = null;

	private String nrCadastroImobiliario = null;

	private String nrLote = null;

	private String nrMatricula = null;

	private String nrQuadra = null;

	private String nrUnidade = null;

	private String numeroLocal = null;

	private BigDecimal pavimentos = null;

	private String quadraGeo = null;

	private String status = null;

	private Integer zoneamentoGeo = null;

	private Integer zoneamentoTrib = null;

	public BigDecimal getAreaCobertComum() {
		return this.areaCobertComum;
	}

	public void setAreaCobertComum(BigDecimal areaCobertComum) {
		this.areaCobertComum = areaCobertComum;
	}

	@XmlElement(name = "area_coberta")
	@JsonProperty(value = "area_coberta")
	@ApiModelProperty(value = "Área coberta")
	public BigDecimal getAreaCoberta() {
		return this.areaCoberta;
	}

	public void setAreaCoberta(BigDecimal areaCoberta) {
		this.areaCoberta = areaCoberta;
	}

	@XmlElement(name = "area_com_preserv")
	@JsonProperty(value = "area_com_preserv", defaultValue = "")
	@ApiModelProperty(value = "Área com preservação da unidade.")
	public BigDecimal getAreaComPreserv() {
		return this.areaComPreserv;
	}

	public void setAreaComPreserv(BigDecimal areaComPreserv) {
		this.areaComPreserv = areaComPreserv;
	}

	@XmlElement(name = "area_com_preserv_comum")
	@JsonProperty(value = "area_com_preserv_comum", defaultValue = "")
	@ApiModelProperty(value = "Área com preservação da unidade.")
	public BigDecimal getAreaComPreservComum() {
		return this.areaComPreservComum;
	}

	public void setAreaComPreservComum(BigDecimal areaComPreservComum) {
		this.areaComPreservComum = areaComPreservComum;
	}

	@XmlElement(name = "area_com_preserv_vinc")
	@JsonProperty(value = "area_com_preserv_vinc", defaultValue = "")
	@ApiModelProperty(value = "Área com preservação vinculada a unidade. Ex.: Área comum do condomínio.")
	public BigDecimal getAreaComPreservVinc() {
		return this.areaComPreservVinc;
	}

	public void setAreaComPreservVinc(BigDecimal areaComPreservVinc) {
		this.areaComPreservVinc = areaComPreservVinc;
	}

	@XmlElement(name = "area_descoberta_comum")
	@JsonProperty(value = "area_descoberta_comum")
	@ApiModelProperty(value = "Área de Uso Comum Descoberta da Edificação")
	public BigDecimal getAreaDescobertComum() {
		return this.areaDescobertComum;
	}

	public void setAreaDescobertComum(BigDecimal areaDescobertComum) {
		this.areaDescobertComum = areaDescobertComum;
	}

	@XmlElement(name = "area_descoberta")
	@JsonProperty(value = "area_descoberta")
	@ApiModelProperty(value = "Área Descoberta da Edificação")
	public BigDecimal getAreaDescoberta() {
		return this.areaDescoberta;
	}

	public void setAreaDescoberta(BigDecimal areaDescoberta) {
		this.areaDescoberta = areaDescoberta;
	}

	@XmlElement(name = "area_descoberta_secundaria")
	@JsonProperty(value = "area_descoberta_secundaria")
	@ApiModelProperty(value = "Área Descoberta Secundária")
	public BigDecimal getAreaDescobertaSecundaria() {
		return this.areaDescobertaSecundaria;
	}

	public void setAreaDescobertaSecundaria(BigDecimal areaDescobertaSecundaria) {
		this.areaDescobertaSecundaria = areaDescobertaSecundaria;
	}

	@XmlElement(name = "area_mezanino")
	@JsonProperty(value = "area_mezanino")
	@ApiModelProperty(value = "Área do mezanino")
	public BigDecimal getAreaMezanino() {
		return this.areaMezanino;
	}

	public void setAreaMezanino(BigDecimal areaMezanino) {
		this.areaMezanino = areaMezanino;
	}

	@XmlElement(name = "area_mezanino_comum")
	@JsonProperty(value = "area_mezanino_comum")
	@ApiModelProperty(value = "Área de Uso Comum do mezanino")
	public BigDecimal getAreaMezaninoComum() {
		return this.areaMezaninoComum;
	}

	public void setAreaMezaninoComum(BigDecimal areaMezaninoComum) {
		this.areaMezaninoComum = areaMezaninoComum;
	}

	@XmlElement(name = "area_sem_preserv")
	@JsonProperty(value = "area_sem_preserv", defaultValue = "")
	@ApiModelProperty(value = "Área sem preservação da unidade.")
	public BigDecimal getAreaSemPreserv() {
		return this.areaSemPreserv;
	}

	public void setAreaSemPreserv(BigDecimal areaSemPreserv) {
		this.areaSemPreserv = areaSemPreserv;
	}

	@XmlElement(name = "area_sem_preserv_comum")
	@JsonProperty(value = "area_sem_preserv_comum", defaultValue = "")
	@ApiModelProperty(value = "Área de Uso Comum sem preservação.")
	public BigDecimal getAreaSemPreservComum() {
		return this.areaSemPreservComum;
	}

	public void setAreaSemPreservComum(BigDecimal areaSemPreservComum) {
		this.areaSemPreservComum = areaSemPreservComum;
	}

	@XmlElement(name = "area_sem_preserv_vinc")
	@JsonProperty(value = "area_sem_preserv_vinc", defaultValue = "")
	@ApiModelProperty(value = "Área sem preservação vinculada a unidade.")
	public BigDecimal getAreaSemPreservVinc() {
		return this.areaSemPreservVinc;
	}

	public void setAreaSemPreservVinc(BigDecimal areaSemPreservVinc) {
		this.areaSemPreservVinc = areaSemPreservVinc;
	}

	@XmlElement(name = "bairro_local")
	@JsonProperty(value = "bairro_local", defaultValue = "")
	@ApiModelProperty(value = "Nome do Bairro")
	public String getBairroLocal() {
		return this.bairroLocal;
	}

	public void setBairroLocal(String bairroLocal) {
		this.bairroLocal = bairroLocal;
	}

	@XmlElement(name = "cd_distrito")
	@JsonProperty(value = "cd_distrito", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Distrito")
	public String getCdDistrito() {
		return this.cdDistrito;
	}

	public void setCdDistrito(String cdDistrito) {
		this.cdDistrito = cdDistrito;
	}

	@XmlElement(name = "cd_loteamento")
	@JsonProperty(value = "cd_loteamento", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Loteamento")
	public String getCdLoteamento() {
		return this.cdLoteamento;
	}

	public void setCdLoteamento(String cdLoteamento) {
		this.cdLoteamento = cdLoteamento;
	}

	@XmlElement(name = "cd_setor")
	@JsonProperty(value = "cd_setor", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Setor")
	public String getCdSetor() {
		return this.cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	@XmlElement(name = "cod_prop_princ")
	@JsonProperty(value = "cod_prop_princ", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Proprietário Principal")
	public String getCodPropPrinc() {
		return this.codPropPrinc;
	}

	public void setCodPropPrinc(String codPropPrinc) {
		this.codPropPrinc = codPropPrinc;
	}

	@XmlElement(name = "cod_tipo_contribuinte")
	@JsonProperty(value = "cod_tipo_contribuinte", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Tipo de Contribuinte")
	public String getCodTipoContribuinte() {
		return this.codTipoContribuinte;
	}

	public void setCodTipoContribuinte(String codTipoContribuinte) {
		this.codTipoContribuinte = codTipoContribuinte;
	}

	@XmlElement(name = "cod_tipo_imovel")
	@JsonProperty(value = "cod_tipo_imovel", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Tipo do Imóvel")
	public String getCodTipoImovel() {
		return this.codTipoImovel;
	}

	public void setCodTipoImovel(String codTipoImovel) {
		this.codTipoImovel = codTipoImovel;
	}

	@XmlElement(name = "codigo_logradouro_local")
	@JsonProperty(value = "codigo_logradouro_local", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Logradouro")
	public String getCodigoLogradouroLocal() {
		return this.codigoLogradouroLocal;
	}

	public void setCodigoLogradouroLocal(String codigoLogradouroLocal) {
		this.codigoLogradouroLocal = codigoLogradouroLocal;
	}

	@XmlElement(name = "complemento_local")
	@JsonProperty(value = "complemento_local", defaultValue = "")
	@ApiModelProperty(value = "Complemento do Endereço")
	public String getComplementoLocal() {
		return this.complementoLocal;
	}

	public void setComplementoLocal(String complementoLocal) {
		this.complementoLocal = complementoLocal;
	}

	@XmlElement(name = "convenio_agua")
	@JsonProperty(value = "convenio_agua", defaultValue = "")
	@ApiModelProperty(value = "Registro da Água")
	public String getConvenioAgua() {
		return this.convenioAgua;
	}

	public void setConvenioAgua(String convenioAgua) {
		this.convenioAgua = convenioAgua;
	}

	@XmlElement(name = "convenio_energia")
	@JsonProperty(value = "convenio_energia", defaultValue = "")
	@ApiModelProperty(value = "Registro da Energia")
	public String getConvenioEnergia() {
		return this.convenioEnergia;
	}

	public void setConvenioEnergia(String convenioEnergia) {
		this.convenioEnergia = convenioEnergia;
	}

	@XmlElement(name = "id_pessoa")
	@JsonProperty(value = "id_pessoa", defaultValue = "")
	@ApiModelProperty(value = "Identificador Único da Pessoa")
	public Integer getIdpessoa() {
		return this.idPessoa;
	}

	public void setIdpessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	@XmlElement(name = "lote_geo")
	@JsonProperty(value = "lote_geo", defaultValue = "")
	@ApiModelProperty(value = "Lote GEO")
	public String getLoteGeo() {
		return this.loteGeo;
	}

	public void setLoteGeo(String loteGeo) {
		this.loteGeo = loteGeo;
	}

	@XmlElement(name = "nr_cadastro_imobiliario")
	@JsonProperty(value = "nr_cadastro_imobiliario", defaultValue = "")
	@ApiModelProperty(value = "Nro Cadastro do Imóvel (matricula)")
	public String getNrCadastroImobiliario() {
		return this.nrCadastroImobiliario;
	}

	public void setNrCadastroImobiliario(String nrCadastroImobiliario) {
		this.nrCadastroImobiliario = nrCadastroImobiliario;
	}

	@XmlElement(name = "nr_lote")
	@JsonProperty(value = "nr_lote", defaultValue = "")
	@ApiModelProperty(value = "Nro do Lote")
	public String getNrLote() {
		return this.nrLote;
	}

	public void setNrLote(String nrLote) {
		this.nrLote = nrLote;
	}

	@XmlElement(name = "nr_matricula")
	@JsonProperty(value = "nr_matricula", defaultValue = "")
	@ApiModelProperty(value = "Nro Matricula do Imóvel")
	public String getNrMatricula() {
		return this.nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	@XmlElement(name = "nr_quadra")
	@JsonProperty(value = "nr_quadra", defaultValue = "")
	@ApiModelProperty(value = "Nro da Quadra")
	public String getNrQuadra() {
		return this.nrQuadra;
	}

	public void setNrQuadra(String nrQuadra) {
		this.nrQuadra = nrQuadra;
	}

	@XmlElement(name = "nr_unidade")
	@JsonProperty(value = "nr_unidade", defaultValue = "")
	@ApiModelProperty(value = "Nro da Unidade")
	public String getNrUnidade() {
		return this.nrUnidade;
	}

	public void setNrUnidade(String nrUnidade) {
		this.nrUnidade = nrUnidade;
	}

	@XmlElement(name = "numero_local")
	@JsonProperty(value = "numero_local", defaultValue = "")
	@ApiModelProperty(value = "Nro do Local")
	public String getNumeroLocal() {
		return this.numeroLocal;
	}

	public void setNumeroLocal(String numeroLocal) {
		this.numeroLocal = numeroLocal;
	}

	@XmlElement(name = "pavimentos")
	@JsonProperty(value = "pavimentos", defaultValue = "")
	@ApiModelProperty(value = "Pavimentos")
	public BigDecimal getPavimentos() {
		return this.pavimentos;
	}

	public void setPavimentos(BigDecimal pavimentos) {
		this.pavimentos = pavimentos;
	}

	@XmlElement(name = "quadra_geo")
	@JsonProperty(value = "quadra_geo", defaultValue = "")
	@ApiModelProperty(value = "Quadra do GEO")
	public String getQuadraGeo() {
		return this.quadraGeo;
	}

	public void setQuadraGeo(String quadraGeo) {
		this.quadraGeo = quadraGeo;
	}

	@XmlElement(name = "status")
	@JsonProperty(value = "status", defaultValue = "")
	@ApiModelProperty(value = "Status do Imóvel")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name = "zoneamento_geo")
	@JsonProperty(value = "zoneamento_geo", defaultValue = "")
	@ApiModelProperty(value = "")
	public Integer getZoneamentoGeo() {
		return this.zoneamentoGeo;
	}

	public void setZoneamentoGeo(Integer zoneamentoGeo) {
		this.zoneamentoGeo = zoneamentoGeo;
	}

	@XmlElement(name = "zoneamento_trib")
	@JsonProperty(value = "zoneamento_trib", defaultValue = "")
	@ApiModelProperty(value = "Zoneamento Tributário")
	public Integer getZoneamentoTrib() {
		return this.zoneamentoTrib;
	}

	public void setZoneamentoTrib(Integer zoneamentoTrib) {
		this.zoneamentoTrib = zoneamentoTrib;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwImovel {\n");

		sb.append("    areaCobertComum: ").append(toIndentedString(areaCobertComum)).append("\n");
		sb.append("    areaCoberta: ").append(toIndentedString(areaCoberta)).append("\n");
		sb.append("    areaComPreserv: ").append(toIndentedString(areaComPreserv)).append("\n");
		sb.append("    areaComPreservComum: ").append(toIndentedString(areaComPreservComum)).append("\n");
		sb.append("    areaComPreservVinc: ").append(toIndentedString(areaComPreservVinc)).append("\n");
		sb.append("    areaDescobertaComum: ").append(toIndentedString(areaDescobertComum)).append("\n");
		sb.append("    areaDescoberta: ").append(toIndentedString(areaDescoberta)).append("\n");
		sb.append("    areaDescobertaSecundaria: ").append(toIndentedString(areaDescobertaSecundaria)).append("\n");
		sb.append("    areaMezanino: ").append(toIndentedString(areaMezanino)).append("\n");
		sb.append("    areaMezaninoComum: ").append(toIndentedString(areaMezaninoComum)).append("\n");
		sb.append("    areaSemPreserv: ").append(toIndentedString(areaSemPreserv)).append("\n");
		sb.append("    areaSemPreservComum: ").append(toIndentedString(areaSemPreservComum)).append("\n");
		sb.append("    areaSemPreservVinc: ").append(toIndentedString(areaSemPreservVinc)).append("\n");
		sb.append("    bairroLocal: ").append(toIndentedString(bairroLocal)).append("\n");
		sb.append("    cdDistrito: ").append(toIndentedString(cdDistrito)).append("\n");
		sb.append("    cdLoteamento: ").append(toIndentedString(cdLoteamento)).append("\n");
		sb.append("    cdSetor: ").append(toIndentedString(cdSetor)).append("\n");
		sb.append("    codPropPrinc: ").append(toIndentedString(codPropPrinc)).append("\n");
		sb.append("    codTipoContribuinte: ").append(toIndentedString(codTipoContribuinte)).append("\n");
		sb.append("    codTipoImovel: ").append(toIndentedString(codTipoImovel)).append("\n");
		sb.append("    codigoLogradouroLocal: ").append(toIndentedString(codigoLogradouroLocal)).append("\n");
		sb.append("    complementoLocal: ").append(toIndentedString(complementoLocal)).append("\n");
		sb.append("    convenioAgua: ").append(toIndentedString(convenioAgua)).append("\n");
		sb.append("    convenioEnergia: ").append(toIndentedString(convenioEnergia)).append("\n");
		sb.append("    idPessoa: ").append(toIndentedString(idPessoa)).append("\n");
		sb.append("    loteGeo: ").append(toIndentedString(loteGeo)).append("\n");
		sb.append("    nrCadastroImobiliario: ").append(toIndentedString(nrCadastroImobiliario)).append("\n");
		sb.append("    nrLote: ").append(toIndentedString(nrLote)).append("\n");
		sb.append("    nrMatricula: ").append(toIndentedString(nrMatricula)).append("\n");
		sb.append("    nrQuadra: ").append(toIndentedString(nrQuadra)).append("\n");
		sb.append("    nrUnidade: ").append(toIndentedString(nrUnidade)).append("\n");
		sb.append("    numeroLocal: ").append(toIndentedString(numeroLocal)).append("\n");
		sb.append("    pavimentos: ").append(toIndentedString(pavimentos)).append("\n");
		sb.append("    quadraGeo: ").append(toIndentedString(quadraGeo)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    zoneamentoGeo: ").append(toIndentedString(zoneamentoGeo)).append("\n");
		sb.append("    zoneamentoTrib: ").append(toIndentedString(zoneamentoTrib)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
