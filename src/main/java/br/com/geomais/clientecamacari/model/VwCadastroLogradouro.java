package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwCadastroLogradouro {

	private String ocupacao = null;

	private String paredes = null;

	private String pedologia = null;

	private String propriedade = null;

	private String revFachPrinc = null;

	private String segmento = null;

	private String situacao = null;

	private String situacaoImovel = null;

	private String tipologia = null;

	private String topografia = null;

	private String acabFachPrinc = null;

	private String alinhamento = null;

	private String categoriaUso = null;

	private String cdBairro = null;

	private String cdDistrito = null;

	private String cdLogradouro = null;

	private String cdSetor = null;

	private String cobertura = null;

	private String conservacao = null;

	private String delimitacao = null;

	private String dominio = null;

	private String dominioEdif = null;

	private String esquadrias = null;

	private String estrutura = null;

	private Integer idalterCaracAcabFachPrinc = null;

	private Integer idalterCaracAlinhamento = null;

	private Integer idalterCaracCategoriaUso = null;

	private Integer idalterCaracCobertura = null;

	private Integer idalterCaracConservacao = null;

	private Integer idalterCaracDelimitacao = null;

	private Integer idalterCaracDominio = null;

	private Integer idalterCaracDominioEdif = null;

	private Integer idalterCaracEsquadrias = null;

	private Integer idalterCaracEstrutura = null;

	private Integer idalterCaracOcupacao = null;

	private Integer idalterCaracParedes = null;

	private Integer idalterCaracPedologia = null;

	private Integer idalterCaracPropriedade = null;

	private Integer idalterCaracRevFachPrinc = null;

	private Integer idalterCaracSituacao = null;

	private Integer idalterCaracSituacaoImovel = null;

	private Integer idalterCaracTipologia = null;

	private Integer idalterCaracTopografia = null;

	public VwCadastroLogradouro() {
	}

	@XmlElement(name = "acab_fach_princ")
	@JsonProperty(value = "acab_fach_princ", defaultValue = "")
	@ApiModelProperty(value = "Acabamento Fachada Principal")
	public String getAcabFachPrinc() {
		return this.acabFachPrinc;
	}

	public void setAcabFachPrinc(String acabFachPrinc) {
		this.acabFachPrinc = acabFachPrinc;
	}

	@XmlElement(name = "alinhamento")
	@JsonProperty(value = "alinhamento", defaultValue = "")
	@ApiModelProperty(value = "Alinhamento")
	public String getAlinhamento() {
		return this.alinhamento;
	}

	public void setAlinhamento(String alinhamento) {
		this.alinhamento = alinhamento;
	}

	@XmlElement(name = "categoria_uso")
	@JsonProperty(value = "categoria_uso", defaultValue = "")
	@ApiModelProperty(value = "Categoria de Uso")
	public String getCategoriaUso() {
		return this.categoriaUso;
	}

	public void setCategoriaUso(String categoriaUso) {
		this.categoriaUso = categoriaUso;
	}

	@XmlElement(name = "cd_bairro")
	@JsonProperty(value = "cd_bairro", defaultValue = "")
	@ApiModelProperty(value = "Código do Bairro")
	public String getCdBairro() {
		return this.cdBairro;
	}

	public void setCdBairro(String cdBairro) {
		this.cdBairro = cdBairro;
	}

	@XmlElement(name = "cd_distrito")
	@JsonProperty(value = "cd_distrito", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Distrito")
	public String getCdDistrito() {
		return this.cdDistrito;
	}

	public void setCdDistrito(String cdDistrito) {
		this.cdDistrito = cdDistrito;
	}

	@XmlElement(name = "cd_logradouro")
	@JsonProperty(value = "cd_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Logradouro")
	public String getCdLogradouro() {
		return this.cdLogradouro;
	}

	public void setCdLogradouro(String cdLogradouro) {
		this.cdLogradouro = cdLogradouro;
	}

	@XmlElement(name = "cd_setor")
	@JsonProperty(value = "cd_setor", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Setor")
	public String getCdSetor() {
		return this.cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	@XmlElement(name = "cobertura")
	@JsonProperty(value = "cobertura", defaultValue = "")
	@ApiModelProperty(value = "Cobertura")
	public String getCobertura() {
		return this.cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	@XmlElement(name = "conservacao")
	@JsonProperty(value = "conservacao", defaultValue = "")
	@ApiModelProperty(value = "Conservação")
	public String getConservacao() {
		return this.conservacao;
	}

	public void setConservacao(String conservacao) {
		this.conservacao = conservacao;
	}

	@XmlElement(name = "delimitacao")
	@JsonProperty(value = "delimitacao", defaultValue = "")
	@ApiModelProperty(value = "Delimitação")
	public String getDelimitacao() {
		return this.delimitacao;
	}

	public void setDelimitacao(String delimitacao) {
		this.delimitacao = delimitacao;
	}

	@XmlElement(name = "dominio")
	@JsonProperty(value = "dominio", defaultValue = "")
	@ApiModelProperty(value = "Dominio")
	public String getDominio() {
		return this.dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	@XmlElement(name = "dominio_edif")
	@JsonProperty(value = "dominio_edif", defaultValue = "")
	@ApiModelProperty(value = "Dominio da Edificação")
	public String getDominioEdif() {
		return this.dominioEdif;
	}

	public void setDominioEdif(String dominioEdif) {
		this.dominioEdif = dominioEdif;
	}

	@XmlElement(name = "esquadrias")
	@JsonProperty(value = "esquadrias", defaultValue = "")
	@ApiModelProperty(value = "Esquadrias")
	public String getEsquadrias() {
		return this.esquadrias;
	}

	public void setEsquadrias(String esquadrias) {
		this.esquadrias = esquadrias;
	}

	@XmlElement(name = "estrutura")
	@JsonProperty(value = "estrutura", defaultValue = "")
	@ApiModelProperty(value = "Estrutura")
	public String getEstrutura() {
		return this.estrutura;
	}

	public void setEstrutura(String estrutura) {
		this.estrutura = estrutura;
	}

	@XmlElement(name = "idalter_carac_acab_fach_princ")
	@JsonProperty(value = "idalter_carac_acab_fach_princ", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Fachada Principal")
	public Integer getIdalterCaracAcabFachPrinc() {
		return this.idalterCaracAcabFachPrinc;
	}

	public void setIdalterCaracAcabFachPrinc(Integer idalterCaracAcabFachPrinc) {
		this.idalterCaracAcabFachPrinc = idalterCaracAcabFachPrinc;
	}

	@XmlElement(name = "idalter_carac_alinhamento")
	@JsonProperty(value = "idalter_carac_alinhamento", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Alinhamento")
	public Integer getIdalterCaracAlinhamento() {
		return this.idalterCaracAlinhamento;
	}

	public void setIdalterCaracAlinhamento(Integer idalterCaracAlinhamento) {
		this.idalterCaracAlinhamento = idalterCaracAlinhamento;
	}

	@XmlElement(name = "idalter_carac_categoria_uso")
	@JsonProperty(value = "idalter_carac_categoria_uso", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Categoria de Uso")
	public Integer getIdalterCaracCategoriaUso() {
		return this.idalterCaracCategoriaUso;
	}

	public void setIdalterCaracCategoriaUso(Integer idalterCaracCategoriaUso) {
		this.idalterCaracCategoriaUso = idalterCaracCategoriaUso;
	}

	@XmlElement(name = "idalter_carac_cobertura")
	@JsonProperty(value = "idalter_carac_cobertura", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Cobertura")
	public Integer getIdalterCaracCobertura() {
		return this.idalterCaracCobertura;
	}

	public void setIdalterCaracCobertura(Integer idalterCaracCobertura) {
		this.idalterCaracCobertura = idalterCaracCobertura;
	}

	@XmlElement(name = "idalter_carac_conservacao")
	@JsonProperty(value = "idalter_carac_conservacao", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Conservação")
	public Integer getIdalterCaracConservacao() {
		return this.idalterCaracConservacao;
	}

	public void setIdalterCaracConservacao(Integer idalterCaracConservacao) {
		this.idalterCaracConservacao = idalterCaracConservacao;
	}

	@XmlElement(name = "idalter_carac_delimitacao")
	@JsonProperty(value = "idalter_carac_delimitacao", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Delimitação")
	public Integer getIdalterCaracDelimitacao() {
		return this.idalterCaracDelimitacao;
	}

	public void setIdalterCaracDelimitacao(Integer idalterCaracDelimitacao) {
		this.idalterCaracDelimitacao = idalterCaracDelimitacao;
	}

	@XmlElement(name = "idalter_carac_dominio")
	@JsonProperty(value = "idalter_carac_dominio", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Domínio")
	public Integer getIdalterCaracDominio() {
		return this.idalterCaracDominio;
	}

	public void setIdalterCaracDominio(Integer idalterCaracDominio) {
		this.idalterCaracDominio = idalterCaracDominio;
	}

	@XmlElement(name = "idalter_carac_dominio_edif")
	@JsonProperty(value = "idalter_carac_dominio_edif", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Domínio da Edificação")
	public Integer getIdalterCaracDominioEdif() {
		return this.idalterCaracDominioEdif;
	}

	public void setIdalterCaracDominioEdif(Integer idalterCaracDominioEdif) {
		this.idalterCaracDominioEdif = idalterCaracDominioEdif;
	}

	@XmlElement(name = "idalter_carac_esquadrias")
	@JsonProperty(value = "idalter_carac_esquadrias", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Esquadrias")
	public Integer getIdalterCaracEsquadrias() {
		return this.idalterCaracEsquadrias;
	}

	public void setIdalterCaracEsquadrias(Integer idalterCaracEsquadrias) {
		this.idalterCaracEsquadrias = idalterCaracEsquadrias;
	}

	@XmlElement(name = "idalter_carac_estrutura")
	@JsonProperty(value = "idalter_carac_estrutura", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Estrutura")
	public Integer getIdalterCaracEstrutura() {
		return this.idalterCaracEstrutura;
	}

	public void setIdalterCaracEstrutura(Integer idalterCaracEstrutura) {
		this.idalterCaracEstrutura = idalterCaracEstrutura;
	}

	@XmlElement(name = "idalter_carac_ocupacao")
	@JsonProperty(value = "idalter_carac_ocupacao", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Ocupação")
	public Integer getIdalterCaracOcupacao() {
		return this.idalterCaracOcupacao;
	}

	public void setIdalterCaracOcupacao(Integer idalterCaracOcupacao) {
		this.idalterCaracOcupacao = idalterCaracOcupacao;
	}

	@XmlElement(name = "idalter_carac_paredes")
	@JsonProperty(value = "idalter_carac_paredes", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Paredes")
	public Integer getIdalterCaracParedes() {
		return this.idalterCaracParedes;
	}

	public void setIdalterCaracParedes(Integer idalterCaracParedes) {
		this.idalterCaracParedes = idalterCaracParedes;
	}

	@XmlElement(name = "idalter_carac_pedologia")
	@JsonProperty(value = "idalter_carac_pedologia", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Pedologia")
	public Integer getIdalterCaracPedologia() {
		return this.idalterCaracPedologia;
	}

	public void setIdalterCaracPedologia(Integer idalterCaracPedologia) {
		this.idalterCaracPedologia = idalterCaracPedologia;
	}

	@XmlElement(name = "idalter_carac_propriedade")
	@JsonProperty(value = "idalter_carac_propriedade", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Propriedade")
	public Integer getIdalterCaracPropriedade() {
		return this.idalterCaracPropriedade;
	}

	public void setIdalterCaracPropriedade(Integer idalterCaracPropriedade) {
		this.idalterCaracPropriedade = idalterCaracPropriedade;
	}

	@XmlElement(name = "idalter_carac_rev_fach_princ")
	@JsonProperty(value = "idalter_carac_rev_fach_princ", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Revestimento Fachada Principal")
	public Integer getIdalterCaracRevFachPrinc() {
		return this.idalterCaracRevFachPrinc;
	}

	public void setIdalterCaracRevFachPrinc(Integer idalterCaracRevFachPrinc) {
		this.idalterCaracRevFachPrinc = idalterCaracRevFachPrinc;
	}

	@XmlElement(name = "idalter_carac_situacao")
	@JsonProperty(value = "idalter_carac_situacao", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Situação")
	public Integer getIdalterCaracSituacao() {
		return this.idalterCaracSituacao;
	}

	public void setIdalterCaracSituacao(Integer idalterCaracSituacao) {
		this.idalterCaracSituacao = idalterCaracSituacao;
	}

	@XmlElement(name = "idalter_carac_situacao_imovel")
	@JsonProperty(value = "idalter_carac_situacao_imovel", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Situação do Imóvel")
	public Integer getIdalterCaracSituacaoImovel() {
		return this.idalterCaracSituacaoImovel;
	}

	public void setIdalterCaracSituacaoImovel(Integer idalterCaracSituacaoImovel) {
		this.idalterCaracSituacaoImovel = idalterCaracSituacaoImovel;
	}

	@XmlElement(name = "idalter_carac_tipologia")
	@JsonProperty(value = "idalter_carac_tipologia", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Tipologia")
	public Integer getIdalterCaracTipologia() {
		return this.idalterCaracTipologia;
	}

	public void setIdalterCaracTipologia(Integer idalterCaracTipologia) {
		this.idalterCaracTipologia = idalterCaracTipologia;
	}

	@XmlElement(name = "idalter_carac_topografia")
	@JsonProperty(value = "idalter_carac_topografia", defaultValue = "")
	@ApiModelProperty(value = "Identificador da Caracteristica Topografia")
	public Integer getIdalterCaracTopografia() {
		return this.idalterCaracTopografia;
	}

	public void setIdalterCaracTopografia(Integer idalterCaracTopografia) {
		this.idalterCaracTopografia = idalterCaracTopografia;
	}

	@XmlElement(name = "ocupacao")
	@JsonProperty(value = "ocupacao", defaultValue = "")
	@ApiModelProperty(value = "Ocupação")
	public String getOcupacao() {
		return this.ocupacao;
	}

	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}

	@XmlElement(name = "paredes")
	@JsonProperty(value = "paredes", defaultValue = "")
	@ApiModelProperty(value = "Paredes")
	public String getParedes() {
		return this.paredes;
	}

	public void setParedes(String paredes) {
		this.paredes = paredes;
	}

	@XmlElement(name = "pedologia")
	@JsonProperty(value = "pedologia", defaultValue = "")
	@ApiModelProperty(value = "Pedologia")
	public String getPedologia() {
		return this.pedologia;
	}

	public void setPedologia(String pedologia) {
		this.pedologia = pedologia;
	}

	@XmlElement(name = "propriedade")
	@JsonProperty(value = "propriedade", defaultValue = "")
	@ApiModelProperty(value = "Propriedade")
	public String getPropriedade() {
		return this.propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}

	@XmlElement(name = "revFachPrinc")
	@JsonProperty(value = "revFachPrinc", defaultValue = "")
	@ApiModelProperty(value = "Revestimento Fachada Principal")
	public String getRevFachPrinc() {
		return this.revFachPrinc;
	}

	public void setRevFachPrinc(String revFachPrinc) {
		this.revFachPrinc = revFachPrinc;
	}

	@XmlElement(name = "segmento")
	@JsonProperty(value = "segmento", defaultValue = "")
	@ApiModelProperty(value = "Segmento")
	public String getSegmento() {
		return this.segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	@XmlElement(name = "situacao")
	@JsonProperty(value = "situacao", defaultValue = "")
	@ApiModelProperty(value = "Situação")
	public String getSituacao() {
		return this.situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@XmlElement(name = "situacaoImovel")
	@JsonProperty(value = "situacaoImovel", defaultValue = "")
	@ApiModelProperty(value = "Situação do Logradouro")
	public String getSituacaoImovel() {
		return this.situacaoImovel;
	}

	public void setSituacaoImovel(String situacaoImovel) {
		this.situacaoImovel = situacaoImovel;
	}

	@XmlElement(name = "tipologia")
	@JsonProperty(value = "tipologia", defaultValue = "")
	@ApiModelProperty(value = "Tipologia")
	public String getTipologia() {
		return this.tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	@XmlElement(name = "topografia")
	@JsonProperty(value = "topografia", defaultValue = "")
	@ApiModelProperty(value = "Topografia")
	public String getTopografia() {
		return this.topografia;
	}

	public void setTopografia(String topografia) {
		this.topografia = topografia;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwCadastroLogradouro {\n");

		sb.append("    ocupacao: ").append(toIndentedString(ocupacao)).append("\n");
		sb.append("    paredes: ").append(toIndentedString(paredes)).append("\n");
		sb.append("    pedologia: ").append(toIndentedString(pedologia)).append("\n");
		sb.append("    propriedade: ").append(toIndentedString(propriedade)).append("\n");
		sb.append("    revFachPrinc: ").append(toIndentedString(revFachPrinc)).append("\n");
		sb.append("    segmento: ").append(toIndentedString(segmento)).append("\n");
		sb.append("    situacao: ").append(toIndentedString(situacao)).append("\n");
		sb.append("    situacaoImovel: ").append(toIndentedString(situacaoImovel)).append("\n");
		sb.append("    tipologia: ").append(toIndentedString(tipologia)).append("\n");
		sb.append("    topografia: ").append(toIndentedString(topografia)).append("\n");
		sb.append("    acabFachPrinc: ").append(toIndentedString(acabFachPrinc)).append("\n");
		sb.append("    alinhamento: ").append(toIndentedString(alinhamento)).append("\n");
		sb.append("    categoriaUso: ").append(toIndentedString(categoriaUso)).append("\n");
		sb.append("    cdBairro: ").append(toIndentedString(cdBairro)).append("\n");
		sb.append("    cdDistrito: ").append(toIndentedString(cdDistrito)).append("\n");
		sb.append("    cdLogradouro: ").append(toIndentedString(cdLogradouro)).append("\n");
		sb.append("    cdSetor: ").append(toIndentedString(cdSetor)).append("\n");
		sb.append("    cobertura: ").append(toIndentedString(cobertura)).append("\n");
		sb.append("    conservacao: ").append(toIndentedString(conservacao)).append("\n");
		sb.append("    delimitacao: ").append(toIndentedString(delimitacao)).append("\n");
		sb.append("    dominio: ").append(toIndentedString(dominio)).append("\n");
		sb.append("    dominioEdif: ").append(toIndentedString(dominioEdif)).append("\n");
		sb.append("    esquadrias: ").append(toIndentedString(esquadrias)).append("\n");
		sb.append("    estrutura: ").append(toIndentedString(estrutura)).append("\n");
		sb.append("    idalterCaracAcabFachPrinc: ").append(toIndentedString(idalterCaracAcabFachPrinc)).append("\n");
		sb.append("    idalterCaracAlinhamento: ").append(toIndentedString(idalterCaracAlinhamento)).append("\n");
		sb.append("    idalterCaracCategoriaUso: ").append(toIndentedString(idalterCaracCategoriaUso)).append("\n");
		sb.append("    idalterCaracCobertura: ").append(toIndentedString(idalterCaracCobertura)).append("\n");
		sb.append("    idalterCaracConservacao: ").append(toIndentedString(idalterCaracConservacao)).append("\n");
		sb.append("    idalterCaracDelimitacao: ").append(toIndentedString(idalterCaracDelimitacao)).append("\n");
		sb.append("    idalterCaracDominio: ").append(toIndentedString(idalterCaracDominio)).append("\n");
		sb.append("    idalterCaracDominioEdif: ").append(toIndentedString(idalterCaracDominioEdif)).append("\n");
		sb.append("    idalterCaracEsquadrias: ").append(toIndentedString(idalterCaracEsquadrias)).append("\n");
		sb.append("    idalterCaracEstrutura: ").append(toIndentedString(idalterCaracEstrutura)).append("\n");
		sb.append("    idalterCaracOcupacao: ").append(toIndentedString(idalterCaracOcupacao)).append("\n");
		sb.append("    idalterCaracParedes: ").append(toIndentedString(idalterCaracParedes)).append("\n");
		sb.append("    idalterCaracPedologia: ").append(toIndentedString(idalterCaracPedologia)).append("\n");
		sb.append("    idalterCaracPropriedade: ").append(toIndentedString(idalterCaracPropriedade)).append("\n");
		sb.append("    idalterCaracRevFachPrinc: ").append(toIndentedString(idalterCaracRevFachPrinc)).append("\n");
		sb.append("    idalterCaracSituacao: ").append(toIndentedString(idalterCaracSituacao)).append("\n");
		sb.append("    idalterCaracSituacaoImovel: ").append(toIndentedString(idalterCaracSituacaoImovel)).append("\n");
		sb.append("    idalterCaracTipologia: ").append(toIndentedString(idalterCaracTipologia)).append("\n");
		sb.append("    idalterCaracTopografia: ").append(toIndentedString(idalterCaracTopografia)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
