package br.com.geomais.clientecamacari.model;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CollectionVwCadastroLogradouro {

	@ApiModelProperty(value = "Exibe a quantidade de registros pulados/ignorados.")
	private Integer skip = null;
	@ApiModelProperty(value = "Exibe a quantidade de registros obtidos.")
	private Integer count = null;
	@ApiModelProperty(value = "Exibe a quantidade de registros existentes para o filtro aplicado.")
	private Integer totalCount = null;
	@ApiModelProperty(value = "")
	private List<VwCadastroLogradouro> items = new ArrayList<VwCadastroLogradouro>();

	/**
	 * Exibe a quantidade de registros pulados/ignorados.
	 * 
	 * @return skip
	 **/
	public Integer getSkip() {
		return skip;
	}

	public void setSkip(Integer skip) {
		this.skip = skip;
	}

	public CollectionVwCadastroLogradouro skip(Integer skip) {
		this.skip = skip;
		return this;
	}

	/**
	 * Exibe a quantidade de registros obtidos.
	 * 
	 * @return count
	 **/
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public CollectionVwCadastroLogradouro count(Integer count) {
		this.count = count;
		return this;
	}

	/**
	 * Exibe a quantidade de registros existentes para o filtro aplicado.
	 * 
	 * @return totalCount
	 **/
	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public CollectionVwCadastroLogradouro totalCount(Integer totalCount) {
		this.totalCount = totalCount;
		return this;
	}

	/**
	 * Get items
	 * 
	 * @return items
	 **/
	public List<VwCadastroLogradouro> getItems() {
		return items;
	}

	public void setItems(List<VwCadastroLogradouro> items) {
		this.items = items;
	}

	public CollectionVwCadastroLogradouro items(List<VwCadastroLogradouro> items) {
		this.items = items;
		return this;
	}

	public CollectionVwCadastroLogradouro addItemsItem(VwCadastroLogradouro itemsItem) {
		this.items.add(itemsItem);
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CollectionVwCadastroLogradouro {\n");

		sb.append("    skip: ").append(toIndentedString(skip)).append("\n");
		sb.append("    count: ").append(toIndentedString(count)).append("\n");
		sb.append("    totalCount: ").append(toIndentedString(totalCount)).append("\n");
		sb.append("    items: ").append(toIndentedString(items)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
