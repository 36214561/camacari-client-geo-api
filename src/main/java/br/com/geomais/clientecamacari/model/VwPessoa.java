package br.com.geomais.clientecamacari.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwPessoa {

	private BigDecimal cdCpfCnpj = null;

	private String codPessoa = null;

	private Integer idPessoa = null;

	private String nmPessoaRazaoSocial = null;

	private String tipoPessoa = null;

	@JsonProperty(value = "cd_cpf_cnpj", defaultValue = "")
	@ApiModelProperty(value = "CPF ou CNPJ, conforme tipo de pessoa")
	public BigDecimal getCdCpfCnpj() {
		return this.cdCpfCnpj;
	}

	public void setCdCpfCnpj(BigDecimal cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	@XmlElement(name = "cod_pessoa")
	@JsonProperty(value = "cod_pessoa", defaultValue = "")
	@ApiModelProperty(value = "Código de Pessoa")
	public String getCodPessoa() {
		return this.codPessoa;
	}

	public void setCodPessoa(String codPessoa) {
		this.codPessoa = codPessoa;
	}

	@XmlElement(name = "id_pessoa")
	@JsonProperty(value = "id_pessoa", defaultValue = "")
	@ApiModelProperty(value = "Identificador Único da Pessoa")
	public Integer getIdPessoa() {
		return this.idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	@XmlElement(name = "nm_pessoa_razao_social")
	@JsonProperty(value = "nm_pessoa_razao_social", defaultValue = "")
	@ApiModelProperty(value = "Nome ou Razão Social da Pessoa, conforme tipo de pessoa")
	public String getNmPessoaRazaoSocial() {
		return this.nmPessoaRazaoSocial;
	}

	public void setNmPessoaRazaoSocial(String nmPessoaRazaoSocial) {
		this.nmPessoaRazaoSocial = nmPessoaRazaoSocial;
	}

	@XmlElement(name = "tipo_pessoa")
	@JsonProperty(value = "tipo_pessoa", defaultValue = "")
	@ApiModelProperty(value = "Tipo de pessoa")
	public String getTipoPessoa() {
		return this.tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwPessoa {\n");

		sb.append("    cdCpfCnpj: ").append(toIndentedString(cdCpfCnpj)).append("\n");
		sb.append("    codPessoa: ").append(toIndentedString(codPessoa)).append("\n");
		sb.append("    idPessoa: ").append(toIndentedString(idPessoa)).append("\n");
		sb.append("    nmPessoaRazaoSocial: ").append(toIndentedString(nmPessoaRazaoSocial)).append("\n");
		sb.append("    tipoPessoa: ").append(toIndentedString(tipoPessoa)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
