package br.com.geomais.clientecamacari.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwTestada {

	private String cdDistrito = null;

	private String cdLogradouro = null;

	private String cdSetor = null;

	private Integer cdTestada = null;

	private BigDecimal mdTestada = null;

	private String nrCadastroImobiliario = null;

	private String nrLote = null;

	private String nrQuadra = null;

	private String origem = null;

	private String secao = null;

	@XmlElement(name = "cd_distrito")
	@JsonProperty(value = "cd_distrito", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Distrito")
	public String getCdDistrito() {
		return this.cdDistrito;
	}

	public void setCdDistrito(String cdDistrito) {
		this.cdDistrito = cdDistrito;
	}

	@XmlElement(name = "cd_logradouro")
	@JsonProperty(value = "cd_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Logradouro")
	public String getCdLogradouro() {
		return this.cdLogradouro;
	}

	public void setCdLogradouro(String cdLogradouro) {
		this.cdLogradouro = cdLogradouro;
	}

	@XmlElement(name = "cd_setor")
	@JsonProperty(value = "cd_setor", defaultValue = "")
	@ApiModelProperty(value = "Código do Setor")
	public String getCdSetor() {
		return this.cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	@XmlElement(name = "cd_testada")
	@JsonProperty(value = "cd_testada", defaultValue = "")
	@ApiModelProperty(value = "Código da Testada")
	public Integer getCdTestada() {
		return this.cdTestada;
	}

	public void setCdTestada(Integer cdTestada) {
		this.cdTestada = cdTestada;
	}

	@XmlElement(name = "md_testada")
	@JsonProperty(value = "md_testada", defaultValue = "")
	@ApiModelProperty(value = "Medida da Testada")
	public BigDecimal getMdTestada() {
		return this.mdTestada;
	}

	public void setMdTestada(BigDecimal mdTestada) {
		this.mdTestada = mdTestada;
	}

	@XmlElement(name = "nr_cadastro_imobiliario")
	@JsonProperty(value = "nr_cadastro_imobiliario", defaultValue = "")
	@ApiModelProperty(value = "Nro Cadastro do Imóvel (matricula)")
	public String getNrCadastroImobiliario() {
		return this.nrCadastroImobiliario;
	}

	public void setNrCadastroImobiliario(String nrCadastroImobiliario) {
		this.nrCadastroImobiliario = nrCadastroImobiliario;
	}

	@XmlElement(name = "nr_lote")
	@JsonProperty(value = "nr_lote", defaultValue = "")
	@ApiModelProperty(value = "Nro do lote")
	public String getNrLote() {
		return this.nrLote;
	}

	public void setNrLote(String nrLote) {
		this.nrLote = nrLote;
	}

	@XmlElement(name = "nr_quadra")
	@JsonProperty(value = "nr_quadra", defaultValue = "")
	@ApiModelProperty(value = "Nro da Quadra")
	public String getNrQuadra() {
		return this.nrQuadra;
	}

	public void setNrQuadra(String nrQuadra) {
		this.nrQuadra = nrQuadra;
	}

	@XmlElement(name = "origem")
	@JsonProperty(value = "origem", defaultValue = "")
	@ApiModelProperty(value = "Origem da Testada")
	public String getOrigem() {
		return this.origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	@XmlElement(name = "secao")
	@JsonProperty(value = "secao", defaultValue = "")
	@ApiModelProperty(value = "Seção")
	public String getSecao() {
		return this.secao;
	}

	public void setSecao(String secao) {
		this.secao = secao;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwTestada {\n");

		sb.append("    cdDistrito: ").append(toIndentedString(cdDistrito)).append("\n");
		sb.append("    cdLogradouro: ").append(toIndentedString(cdLogradouro)).append("\n");
		sb.append("    cdSetor: ").append(toIndentedString(cdSetor)).append("\n");
		sb.append("    cdTestada: ").append(toIndentedString(cdTestada)).append("\n");
		sb.append("    mdTestada: ").append(toIndentedString(mdTestada)).append("\n");
		sb.append("    nrCadastroImobiliario: ").append(toIndentedString(nrCadastroImobiliario)).append("\n");
		sb.append("    nrLote: ").append(toIndentedString(nrLote)).append("\n");
		sb.append("    nrQuadra: ").append(toIndentedString(nrQuadra)).append("\n");
		sb.append("    origem: ").append(toIndentedString(origem)).append("\n");
		sb.append("    secao: ").append(toIndentedString(secao)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
