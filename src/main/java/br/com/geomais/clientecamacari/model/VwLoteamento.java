package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwLoteamento {

	private String cdBairro = null;

	private String cdLoteamento = null;

	private String nmLoteamento = null;

	@XmlElement(name = "cd_bairro")
	@JsonProperty(value = "cd_bairro", defaultValue = "")
	@ApiModelProperty(value = "Código do bairro")
	public String getCdBairro() {
		return this.cdBairro;
	}

	public void setCdBairro(String cdBairro) {
		this.cdBairro = cdBairro;
	}

	@XmlElement(name = "cd_loteamento")
	@JsonProperty(value = "cd_loteamento", defaultValue = "")
	@ApiModelProperty(value = "Código do Loteamento")
	public String getCdLoteamento() {
		return this.cdLoteamento;
	}

	public void setCdLoteamento(String cdLoteamento) {
		this.cdLoteamento = cdLoteamento;
	}

	@XmlElement(name = "nm_loteamento")
	@JsonProperty(value = "nm_loteamento", defaultValue = "")
	@ApiModelProperty(value = "Nome do Loteamento")
	public String getNmLoteamento() {
		return this.nmLoteamento;
	}

	public void setNmLoteamento(String nmLoteamento) {
		this.nmLoteamento = nmLoteamento;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwLoteamento {\n");

		sb.append("    cdBairro: ").append(toIndentedString(cdBairro)).append("\n");
		sb.append("    cdLoteamento: ").append(toIndentedString(cdLoteamento)).append("\n");
		sb.append("    nmLoteamento: ").append(toIndentedString(nmLoteamento)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
