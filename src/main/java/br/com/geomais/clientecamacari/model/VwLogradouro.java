package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwLogradouro {

	private String cdLogradouro = null;

	private Integer cdTipoLogradouro = null;

	private Integer idLogradouro = null;

	private String nmLogradouro = null;

	@XmlElement(name = "cd_logradouro")
	@JsonProperty(value = "cd_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Logradouro")
	public String getCdLogradouro() {
		return this.cdLogradouro;
	}

	public void setCdLogradouro(String cdLogradouro) {
		this.cdLogradouro = cdLogradouro;
	}

	@XmlElement(name = "cd_tipo_logradouro")
	@JsonProperty(value = "cd_tipo_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Tipo de Logradouro")
	public Integer getCdTipoLogradouro() {
		return this.cdTipoLogradouro;
	}

	public void setCdTipoLogradouro(Integer cdTipoLogradouro) {
		this.cdTipoLogradouro = cdTipoLogradouro;
	}

	@XmlElement(name = "id_logradouro")
	@JsonProperty(value = "id_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Identificador Único do Logradouro")
	public Integer getIdLogradouro() {
		return this.idLogradouro;
	}

	public void setIdLogradouro(Integer idLogradouro) {
		this.idLogradouro = idLogradouro;
	}

	@XmlElement(name = "nm_logradouro")
	@JsonProperty(value = "nm_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Nome do Logradouro")
	public String getNmLogradouro() {
		return this.nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwLogradouro {\n");

		sb.append("    cdLogradouro: ").append(toIndentedString(cdLogradouro)).append("\n");
		sb.append("    cdTipoLogradouro: ").append(toIndentedString(cdTipoLogradouro)).append("\n");
		sb.append("    idLogradouro: ").append(toIndentedString(idLogradouro)).append("\n");
		sb.append("    nmLogradouro: ").append(toIndentedString(nmLogradouro)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
