package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwLogradouroSecao {

	private String cdBairro = null;

	private String cdDistrito = null;

	private String cdLogradouro = null;

	private String cdSetor = null;

	private Integer cdVlMetroQuadrado = null;

	private Integer idfaceQuadra = null;

	private String nrCep = null;

	private String segmento = null;

	@XmlElement(name = "cd_bairro")
	@JsonProperty(value = "cd_bairro", defaultValue = "")
	@ApiModelProperty(value = "Código do Bairro")
	public String getCdBairro() {
		return this.cdBairro;
	}

	public void setCdBairro(String cdBairro) {
		this.cdBairro = cdBairro;
	}

	@XmlElement(name = "cd_distrito")
	@JsonProperty(value = "cd_distrito", defaultValue = "")
	@ApiModelProperty(value = "Código do Distrito")
	public String getCdDistrito() {
		return this.cdDistrito;
	}

	public void setCdDistrito(String cdDistrito) {
		this.cdDistrito = cdDistrito;
	}

	@XmlElement(name = "cd_logradouro")
	@JsonProperty(value = "cd_logradouro", defaultValue = "")
	@ApiModelProperty(value = "Código do Logradouro")
	public String getCdLogradouro() {
		return this.cdLogradouro;
	}

	public void setCdLogradouro(String cdLogradouro) {
		this.cdLogradouro = cdLogradouro;
	}

	@XmlElement(name = "cd_setor")
	@JsonProperty(value = "cd_setor", defaultValue = "")
	@ApiModelProperty(value = "Código do Setor")
	public String getCdSetor() {
		return this.cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	@XmlElement(name = "cd_vl_metro_quadrado")
	@JsonProperty(value = "cd_vl_metro_quadrado", defaultValue = "")
	@ApiModelProperty(value = "Valor Metro Quadrado")
	public Integer getCdVlMetroQuadrado() {
		return this.cdVlMetroQuadrado;
	}

	public void setCdVlMetroQuadrado(Integer cdVlMetroQuadrado) {
		this.cdVlMetroQuadrado = cdVlMetroQuadrado;
	}

	@XmlElement(name = "idface_quadra")
	@JsonProperty(value = "idface_quadra", defaultValue = "")
	@ApiModelProperty(value = "Identificador Único da Face Quadra")
	public Integer getIdfaceQuadra() {
		return this.idfaceQuadra;
	}

	public void setIdfaceQuadra(Integer idfaceQuadra) {
		this.idfaceQuadra = idfaceQuadra;
	}

	@XmlElement(name = "nr_cep")
	@JsonProperty(value = "nr_cep", defaultValue = "")
	@ApiModelProperty(value = "CEP")
	public String getNrCep() {
		return this.nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	@XmlElement(name = "segmento")
	@JsonProperty(value = "segmento", defaultValue = "")
	@ApiModelProperty(value = "Segmento")
	public String getSegmento() {
		return this.segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwLogradouroSecao {\n");

		sb.append("    cdBairro: ").append(toIndentedString(cdBairro)).append("\n");
		sb.append("    cdDistrito: ").append(toIndentedString(cdDistrito)).append("\n");
		sb.append("    cdLogradouro: ").append(toIndentedString(cdLogradouro)).append("\n");
		sb.append("    cdSetor: ").append(toIndentedString(cdSetor)).append("\n");
		sb.append("    cdVlMetroQuadrado: ").append(toIndentedString(cdVlMetroQuadrado)).append("\n");
		sb.append("    idfaceQuadra: ").append(toIndentedString(idfaceQuadra)).append("\n");
		sb.append("    nrCep: ").append(toIndentedString(nrCep)).append("\n");
		sb.append("    segmento: ").append(toIndentedString(segmento)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
