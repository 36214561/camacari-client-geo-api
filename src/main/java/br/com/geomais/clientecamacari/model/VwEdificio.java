package br.com.geomais.clientecamacari.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class VwEdificio {

	private BigDecimal areaCobertaComum = null;

	private BigDecimal areaCobertaPrivativa = null;

	private BigDecimal areaDescobertaComum = null;

	private BigDecimal areaDescobertaPrivativa = null;

	private BigDecimal areaDescobertaSecundariaComum = null;

	private BigDecimal areaDescobertaSecundariaPrivativa = null;

	private BigDecimal areaMezaninoComum = null;

	private BigDecimal areaMezaninoPrivativa = null;

	private BigDecimal areaTerrenoComPreservComum = null;

	private BigDecimal areaTerrenoComPreservPrivativa = null;

	private BigDecimal areaTerrenoComum = null;

	private BigDecimal areaTerrenoPrivativa = null;

	private BigDecimal areaTerrenoSemPreservComum = null;

	private BigDecimal areaTerrenoSemPreservPrivativa = null;

	private BigDecimal areaTotalCoberta = null;

	private BigDecimal areaTotalDescoberta = null;

	private BigDecimal areaTotalMezanino = null;

	private BigDecimal areaTotalTerreno = null;

	private String cdEdificio = null;

	private String cdQuadra = null;

	private String cdSetor = null;

	private String loteGeo = null;

	private String nmEdificio = null;

	private Integer qtdeGaragem = null;

	private Integer qtdeLoja = null;

	private Integer qtdeSala = null;

	private Integer qtdeUnidade = null;

	private String tipoCondominio = null;

	@XmlElement(name = "area_coberta_comum")
	@JsonProperty(value = "area_coberta_comum", defaultValue = "")
	@ApiModelProperty(value = "Área Coberta de Uso Comum do Edifício")
	public BigDecimal getAreaCobertaComum() {
		return this.areaCobertaComum;
	}

	public void setAreaCobertaComum(BigDecimal areaCobertaComum) {
		this.areaCobertaComum = areaCobertaComum;
	}

	@XmlElement(name = "area_coberta_privativa")
	@JsonProperty(value = "area_coberta_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área Coberta de Uso Privativo do Edifício")
	public BigDecimal getAreaCobertaPrivativa() {
		return this.areaCobertaPrivativa;
	}

	public void setAreaCobertaPrivativa(BigDecimal areaCobertaPrivativa) {
		this.areaCobertaPrivativa = areaCobertaPrivativa;
	}

	@XmlElement(name = "area_descoberta_comum")
	@JsonProperty(value = "area_descoberta_comum", defaultValue = "")
	@ApiModelProperty(value = "Área Descoberta de Uso Comum do Edifício")
	public BigDecimal getAreaDescobertaComum() {
		return this.areaDescobertaComum;
	}

	public void setAreaDescobertaComum(BigDecimal areaDescobertaComum) {
		this.areaDescobertaComum = areaDescobertaComum;
	}

	@XmlElement(name = "area_descoberta_privativa")
	@JsonProperty(value = "area_descoberta_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área Descoberta de Uso Privativo")
	public BigDecimal getAreaDescobertaPrivativa() {
		return this.areaDescobertaPrivativa;
	}

	public void setAreaDescobertaPrivativa(BigDecimal areaDescobertaPrivativa) {
		this.areaDescobertaPrivativa = areaDescobertaPrivativa;
	}

	@XmlElement(name = "area_descoberta_secundaria_comum")
	@JsonProperty(value = "area_descoberta_secundaria_comum", defaultValue = "")
	@ApiModelProperty(value = "Área Secundária Descoberta de Uso Comum do Edifício")
	public BigDecimal getAreaDescobertaSecundariaComum() {
		return this.areaDescobertaSecundariaComum;
	}

	public void setAreaDescobertaSecundariaComum(BigDecimal areaDescobertaSecundariaComum) {
		this.areaDescobertaSecundariaComum = areaDescobertaSecundariaComum;
	}

	@XmlElement(name = "area_descoberta_secundaria_privativa")
	@JsonProperty(value = "area_descoberta_secundaria_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área Secundária Descoberta de Uso Privativo do Edifício")
	public BigDecimal getAreaDescobertaSecundariaPrivativa() {
		return this.areaDescobertaSecundariaPrivativa;
	}

	public void setAreaDescobertaSecundariaPrivativa(BigDecimal areaDescobertaSecundariaPrivativa) {
		this.areaDescobertaSecundariaPrivativa = areaDescobertaSecundariaPrivativa;
	}

	@XmlElement(name = "area_mezanino_comum")
	@JsonProperty(value = "area_mezanino_comum", defaultValue = "")
	@ApiModelProperty(value = "Área do Mezanino de Uso Comum do Edifício")
	public BigDecimal getAreaMezaninoComum() {
		return this.areaMezaninoComum;
	}

	public void setAreaMezaninoComum(BigDecimal areaMezaninoComum) {
		this.areaMezaninoComum = areaMezaninoComum;
	}

	@XmlElement(name = "area_mezanino_privativa")
	@JsonProperty(value = "area_mezanino_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área Secundária Descoberta de Uso Privativo ")
	public BigDecimal getAreaMezaninoPrivativa() {
		return this.areaMezaninoPrivativa;
	}

	public void setAreaMezaninoPrivativa(BigDecimal areaMezaninoPrivativa) {
		this.areaMezaninoPrivativa = areaMezaninoPrivativa;
	}

	@XmlElement(name = "area_terreno_com_preserv_comum")
	@JsonProperty(value = "area_terreno_com_preserv_comum", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno com Preservação de Uso Comum")
	public BigDecimal getAreaTerrenoComPreservComum() {
		return this.areaTerrenoComPreservComum;
	}

	public void setAreaTerrenoComPreservComum(BigDecimal areaTerrenoComPreservComum) {
		this.areaTerrenoComPreservComum = areaTerrenoComPreservComum;
	}

	@XmlElement(name = "area_terreno_com_preserv_privativa")
	@JsonProperty(value = "area_terreno_com_preserv_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno com Preservação de Uso Privativo")
	public BigDecimal getAreaTerrenoComPreservPrivativa() {
		return this.areaTerrenoComPreservPrivativa;
	}

	public void setAreaTerrenoComPreservPrivativa(BigDecimal areaTerrenoComPreservPrivativa) {
		this.areaTerrenoComPreservPrivativa = areaTerrenoComPreservPrivativa;
	}

	@XmlElement(name = "area_terreno_comum")
	@JsonProperty(value = "area_terreno_comum", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno de uso Comum")
	public BigDecimal getAreaTerrenoComum() {
		return this.areaTerrenoComum;
	}

	public void setAreaTerrenoComum(BigDecimal areaTerrenoComum) {
		this.areaTerrenoComum = areaTerrenoComum;
	}

	@XmlElement(name = "area_terreno_privativa")
	@JsonProperty(value = "area_terreno_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno de uso Privativo")
	public BigDecimal getAreaTerrenoPrivativa() {
		return this.areaTerrenoPrivativa;
	}

	public void setAreaTerrenoPrivativa(BigDecimal areaTerrenoPrivativa) {
		this.areaTerrenoPrivativa = areaTerrenoPrivativa;
	}

	@XmlElement(name = "area_terreno_sem_preserv_comum")
	@JsonProperty(value = "area_terreno_sem_preserv_comum", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno sem Preservação de uso Comum")
	public BigDecimal getAreaTerrenoSemPreservComum() {
		return this.areaTerrenoSemPreservComum;
	}

	public void setAreaTerrenoSemPreservComum(BigDecimal areaTerrenoSemPreservComum) {
		this.areaTerrenoSemPreservComum = areaTerrenoSemPreservComum;
	}

	@XmlElement(name = "area_terreno_sem_preserv_privativa")
	@JsonProperty(value = "area_terreno_sem_preserv_privativa", defaultValue = "")
	@ApiModelProperty(value = "Área do Terreno sem Preservação de uso Privativo")
	public BigDecimal getAreaTerrenoSemPreservPrivativa() {
		return this.areaTerrenoSemPreservPrivativa;
	}

	public void setAreaTerrenoSemPreservPrivativa(BigDecimal areaTerrenoSemPreservPrivativa) {
		this.areaTerrenoSemPreservPrivativa = areaTerrenoSemPreservPrivativa;
	}

	@XmlElement(name = "area_total_coberta")
	@JsonProperty(value = "area_total_coberta", defaultValue = "")
	@ApiModelProperty(value = "Área Total Coberta")
	public BigDecimal getAreaTotalCoberta() {
		return this.areaTotalCoberta;
	}

	public void setAreaTotalCoberta(BigDecimal areaTotalCoberta) {
		this.areaTotalCoberta = areaTotalCoberta;
	}

	@XmlElement(name = "area_total_descoberta")
	@JsonProperty(value = "area_total_descoberta", defaultValue = "")
	@ApiModelProperty(value = "Área Total Descoberta")
	public BigDecimal getAreaTotalDescoberta() {
		return this.areaTotalDescoberta;
	}

	public void setAreaTotalDescoberta(BigDecimal areaTotalDescoberta) {
		this.areaTotalDescoberta = areaTotalDescoberta;
	}

	@XmlElement(name = "area_total_mezanino")
	@JsonProperty(value = "area_total_mezanino", defaultValue = "")
	@ApiModelProperty(value = "Área Total Mezanino")
	public BigDecimal getAreaTotalMezanino() {
		return this.areaTotalMezanino;
	}

	public void setAreaTotalMezanino(BigDecimal areaTotalMezanino) {
		this.areaTotalMezanino = areaTotalMezanino;
	}

	@XmlElement(name = "area_total_terreno")
	@JsonProperty(value = "area_total_terreno", defaultValue = "")
	@ApiModelProperty(value = "Área Total do Terreno")
	public BigDecimal getAreaTotalTerreno() {
		return this.areaTotalTerreno;
	}

	public void setAreaTotalTerreno(BigDecimal areaTotalTerreno) {
		this.areaTotalTerreno = areaTotalTerreno;
	}

	@XmlElement(name = "cd_edificio")
	@JsonProperty(value = "cd_edificio", defaultValue = "")
	@ApiModelProperty(value = "Código do Edifício")
	public String getCdEdificio() {
		return this.cdEdificio;
	}

	public void setCdEdificio(String cdEdificio) {
		this.cdEdificio = cdEdificio;
	}

	@XmlElement(name = "cd_quadra")
	@JsonProperty(value = "cd_quadra", defaultValue = "")
	@ApiModelProperty(value = "Cód. da Quadra")
	public String getCdQuadra() {
		return this.cdQuadra;
	}

	public void setCdQuadra(String cdQuadra) {
		this.cdQuadra = cdQuadra;
	}

	@XmlElement(name = "cd_setor")
	@JsonProperty(value = "cd_setor", defaultValue = "")
	@ApiModelProperty(value = "Cód. do Setor")
	public String getCdSetor() {
		return this.cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	@XmlElement(name = "lote_geo")
	@JsonProperty(value = "lote_geo", defaultValue = "")
	@ApiModelProperty(value = "Lote GEO")
	public String getLoteGeo() {
		return this.loteGeo;
	}

	public void setLoteGeo(String loteGeo) {
		this.loteGeo = loteGeo;
	}

	@XmlElement(name = "nm_edificio")
	@JsonProperty(value = "nm_edificio", defaultValue = "")
	@ApiModelProperty(value = "Número Edificio")
	public String getNmEdificio() {
		return this.nmEdificio;
	}

	public void setNmEdificio(String nmEdificio) {
		this.nmEdificio = nmEdificio;
	}

	@XmlElement(name = "qtde_garagem")
	@JsonProperty(value = "qtde_garagem", defaultValue = "")
	@ApiModelProperty(value = "Quantidade de Garagens")
	public Integer getQtdeGaragem() {
		return this.qtdeGaragem;
	}

	public void setQtdeGaragem(Integer qtdeGaragem) {
		this.qtdeGaragem = qtdeGaragem;
	}

	@XmlElement(name = "qtde_loja")
	@JsonProperty(value = "qtde_loja", defaultValue = "")
	@ApiModelProperty(value = "Quantidade de Lojas")
	public Integer getQtdeLoja() {
		return this.qtdeLoja;
	}

	public void setQtdeLoja(Integer qtdeLoja) {
		this.qtdeLoja = qtdeLoja;
	}

	@XmlElement(name = "qtde_sala")
	@JsonProperty(value = "qtde_sala", defaultValue = "")
	@ApiModelProperty(value = "Quantidade de Salas")
	public Integer getQtdeSala() {
		return this.qtdeSala;
	}

	public void setQtdeSala(Integer qtdeSala) {
		this.qtdeSala = qtdeSala;
	}

	@XmlElement(name = "qtde_unidade")
	@JsonProperty(value = "qtde_unidade", defaultValue = "")
	@ApiModelProperty(value = "Quantidade de Unidades")
	public Integer getQtdeUnidade() {
		return this.qtdeUnidade;
	}

	public void setQtdeUnidade(Integer qtdeUnidade) {
		this.qtdeUnidade = qtdeUnidade;
	}

	@XmlElement(name = "tipo_condominio")
	@JsonProperty(value = "tipo_condominio", defaultValue = "")
	@ApiModelProperty(value = "Tipo de Condomínio")
	public String getTipoCondominio() {
		return this.tipoCondominio;
	}

	public void setTipoCondominio(String tipoCondominio) {
		this.tipoCondominio = tipoCondominio;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwEdificio {\n");

		sb.append("    areaCobertaComum: ").append(toIndentedString(areaCobertaComum)).append("\n");
		sb.append("    areaCobertaPrivativa: ").append(toIndentedString(areaCobertaPrivativa)).append("\n");
		sb.append("    areaDescobertaComum: ").append(toIndentedString(areaDescobertaComum)).append("\n");
		sb.append("    areaDescobertaPrivativa: ").append(toIndentedString(areaDescobertaPrivativa)).append("\n");
		sb.append("    areaDescobertaSecundariaComum: ").append(toIndentedString(areaDescobertaSecundariaComum))
				.append("\n");
		sb.append("    areaDescobertaSecundariaPrivativa: ").append(toIndentedString(areaDescobertaSecundariaPrivativa))
				.append("\n");
		sb.append("    areaMezaninoComum: ").append(toIndentedString(areaMezaninoComum)).append("\n");
		sb.append("    areaMezaninoPrivativa: ").append(toIndentedString(areaMezaninoPrivativa)).append("\n");
		sb.append("    areaTerrenoComPreservComum: ").append(toIndentedString(areaTerrenoComPreservComum)).append("\n");
		sb.append("    areaTerrenoComPreservPrivativa: ").append(toIndentedString(areaTerrenoComPreservPrivativa))
				.append("\n");
		sb.append("    areaTerrenoComum: ").append(toIndentedString(areaTerrenoComum)).append("\n");
		sb.append("    areaTerrenoPrivativa: ").append(toIndentedString(areaTerrenoPrivativa)).append("\n");
		sb.append("    areaTerrenoSemPreservComum: ").append(toIndentedString(areaTerrenoSemPreservComum)).append("\n");
		sb.append("    areaTerrenoSemPreservPrivativa: ").append(toIndentedString(areaTerrenoSemPreservPrivativa))
				.append("\n");
		sb.append("    areaTotalCoberta: ").append(toIndentedString(areaTotalCoberta)).append("\n");
		sb.append("    areaTotalDescoberta: ").append(toIndentedString(areaTotalDescoberta)).append("\n");
		sb.append("    areaTotalMezanino: ").append(toIndentedString(areaTotalMezanino)).append("\n");
		sb.append("    areaTotalTerreno: ").append(toIndentedString(areaTotalTerreno)).append("\n");
		sb.append("    cdEdificio: ").append(toIndentedString(cdEdificio)).append("\n");
		sb.append("    cdQuadra: ").append(toIndentedString(cdQuadra)).append("\n");
		sb.append("    cdSetor: ").append(toIndentedString(cdSetor)).append("\n");
		sb.append("    loteGeo: ").append(toIndentedString(loteGeo)).append("\n");
		sb.append("    nmEdificio: ").append(toIndentedString(nmEdificio)).append("\n");
		sb.append("    qtdeGaragem: ").append(toIndentedString(qtdeGaragem)).append("\n");
		sb.append("    qtdeLoja: ").append(toIndentedString(qtdeLoja)).append("\n");
		sb.append("    qtdeSala: ").append(toIndentedString(qtdeSala)).append("\n");
		sb.append("    qtdeUnidade: ").append(toIndentedString(qtdeUnidade)).append("\n");
		sb.append("    tipoCondominio: ").append(toIndentedString(tipoCondominio)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
