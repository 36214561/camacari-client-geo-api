package br.com.geomais.clientecamacari.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

public class ApiResponseMessage {

	private Integer code = null;

	private String message = null;

	private String description = null;

	private String type = null;

	/**
	 * Código do erro
	 * 
	 * @return code
	 **/
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ApiResponseMessage code(Integer code) {
		this.code = code;
		return this;
	}

	/**
	 * Mensagem de erro
	 * 
	 * @return message
	 **/
	@XmlElement
	@ApiModelProperty(value = "Mensagem de erro")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ApiResponseMessage message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Descrição detalhada do erro
	 * 
	 * @return description
	 **/
	@XmlElement
	@ApiModelProperty(value = "Descrição detalhada do erro")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ApiResponseMessage description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * O tipo de Mensagem (Erro, sugestão..)
	 * 
	 * @return type
	 **/
	@XmlElement
	@ApiModelProperty(value = "O tipo de Mensagem (Erro, sugestão..)")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ApiResponseMessage type(String type) {
		this.type = type;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApiResponseMessage {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
