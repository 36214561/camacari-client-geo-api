package br.com.geomais.clientecamacari.model;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CollectionVwPessoa {

	@ApiModelProperty(value = "Exibe a quantidade de registros pulados/ignorados.")
	private Integer skip = null;
	@ApiModelProperty(value = "Exibe a quantidade de registros obtidos.")
	private Integer count = null;
	@ApiModelProperty(value = "Exibe a quantidade de registros existentes para o filtro aplicado.")
	private Integer totalCount = null;
	@ApiModelProperty(value = "")
	private List<VwPessoa> items = new ArrayList<VwPessoa>();

	/**
	 * Exibe a quantidade de registros pulados/ignorados.
	 * 
	 * @return skip
	 **/
	public Integer getSkip() {
		return skip;
	}

	public void setSkip(Integer skip) {
		this.skip = skip;
	}

	public CollectionVwPessoa skip(Integer skip) {
		this.skip = skip;
		return this;
	}

	/**
	 * Exibe a quantidade de registros obtidos.
	 * 
	 * @return count
	 **/
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public CollectionVwPessoa count(Integer count) {
		this.count = count;
		return this;
	}

	/**
	 * Exibe a quantidade de registros existentes para o filtro aplicado.
	 * 
	 * @return totalCount
	 **/
	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public CollectionVwPessoa totalCount(Integer totalCount) {
		this.totalCount = totalCount;
		return this;
	}

	/**
	 * Get items
	 * 
	 * @return items
	 **/
	public List<VwPessoa> getItems() {
		return items;
	}

	public void setItems(List<VwPessoa> items) {
		this.items = items;
	}

	public CollectionVwPessoa items(List<VwPessoa> items) {
		this.items = items;
		return this;
	}

	public CollectionVwPessoa addItemsItem(VwPessoa itemsItem) {
		this.items.add(itemsItem);
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CollectionVwPessoa {\n");

		sb.append("    skip: ").append(toIndentedString(skip)).append("\n");
		sb.append("    count: ").append(toIndentedString(count)).append("\n");
		sb.append("    totalCount: ").append(toIndentedString(totalCount)).append("\n");
		sb.append("    items: ").append(toIndentedString(items)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
