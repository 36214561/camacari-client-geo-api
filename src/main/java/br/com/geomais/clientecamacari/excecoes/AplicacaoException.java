package br.com.geomais.clientecamacari.excecoes;

import java.util.ArrayList;
import java.util.List;

public class AplicacaoException extends Throwable{
	List<DescricaoValue> erros = new ArrayList<>();

	public AplicacaoException() {
		super();
	}
	
	public AplicacaoException(String message) {
		super(message);
	}
	
	public void addErro(DescricaoValue erroExceptionValue) {
		erros.add(erroExceptionValue);
	}
	
	public List<DescricaoValue> getErros() {
		return erros;
	}
}
