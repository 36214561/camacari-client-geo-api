package br.com.geomais.clientecamacari.excecoes;

public class SistemaException extends Exception{
	
	public SistemaException(String message) {
		super(message);
	}
	
	public SistemaException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public SistemaException(Exception e) {
		super("Ocorreu um erro em tempo de execução.", e);
	}
}