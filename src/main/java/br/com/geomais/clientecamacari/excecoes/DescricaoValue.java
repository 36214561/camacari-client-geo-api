package br.com.geomais.clientecamacari.excecoes;

public class DescricaoValue {
	private String mensagem;
	private String detalhe;
	
	public DescricaoValue(String mensagem) {
		super();
		this.mensagem = mensagem;
	}
	
	public DescricaoValue(String mensagem, String detalhe) {
		super();
		this.mensagem = mensagem;
		this.detalhe = detalhe;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public String getDetalhe() {
		return detalhe;
	}
}
