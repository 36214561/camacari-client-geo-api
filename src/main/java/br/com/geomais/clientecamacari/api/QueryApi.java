package br.com.geomais.clientecamacari.api;

import javax.ws.rs.core.Response;

public interface QueryApi {
	public Response executeGet(String params, Integer skip, Integer limit, String authorization);
}
