package br.com.geomais.clientecamacari.api.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class ParserUtil<T> {

	private Class clazz;

	private ParserUtil() {
	}

	public ParserUtil(Class clazz) {
		super();
		this.clazz = clazz;
	}

	public T toObject(InputStream inputStream) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		StringBuffer jsonStrBuffer = new StringBuffer();
		while (br.ready()) {
			jsonStrBuffer.append(br.readLine());
		}
		br.close();
		br = null;

		ObjectMapper objectMapper = new ObjectMapper();
		T retorno = (T) objectMapper.readValue(jsonStrBuffer.toString(), this.clazz);
		return retorno;
	}

	public static String toJson(Object object) throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(object);
		return json;
	}
}
