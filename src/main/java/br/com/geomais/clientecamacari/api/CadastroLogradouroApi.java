package br.com.geomais.clientecamacari.api;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import br.com.geomais.clientecamacari.model.ApiResponseMessage;
import br.com.geomais.clientecamacari.model.CollectionVwCadastroLogradouro;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/")
@Api(value = "/", description = "")
public interface CadastroLogradouroApi extends QueryApi {

	@GET
	@Path("/cadastrologradouros")
	@Produces({ "application/json", "application/xml" })
	@ApiOperation(value = "Obtém uma coleção de registros", tags = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A consulta foi executada com sucesso.", response = CollectionVwCadastroLogradouro.class),
			@ApiResponse(code = 400, message = "A requisição foi mal formada.", response = ApiResponseMessage.class),
			@ApiResponse(code = 401, message = "O schema de autenticação ou as credenciais estão inválidas. Informe credenciais válidas.", response = ApiResponseMessage.class),
			@ApiResponse(code = 403, message = "Acesso bloqueado para este recurso!", response = ApiResponseMessage.class),
			@ApiResponse(code = 500, message = "Ocorreu um erro inesperado no servidor.", response = ApiResponseMessage.class) })
	public Response executeGet(@QueryParam("params") String params, @QueryParam("skip") @DefaultValue("0") Integer skip,
			@QueryParam("limit") @DefaultValue("10") Integer limit, @HeaderParam("Authorization") String authorization);
}
