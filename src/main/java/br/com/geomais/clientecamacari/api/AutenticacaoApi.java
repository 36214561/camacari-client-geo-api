package br.com.geomais.clientecamacari.api;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.geomais.clientecamacari.model.ApiResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Interface que especifica os end-point de autenticação
 * 
 * @author jordani
 * 
 * 
 */
@Path("/")
@Api(value = "/", description = "")
public interface AutenticacaoApi {

	@POST
	@Path("/autenticar")
	@Produces({ "application/json", "application/xml" })
	@ApiOperation(value = "Autentica o usuário para obtenção de token de acesso.", tags = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A autenticação foi executada com sucesso.", response = ApiResponseMessage.class),
			@ApiResponse(code = 401, message = "O schema de autenticação ou as credenciais estão inválidas. Informe credenciais válidas.", response = ApiResponseMessage.class),
			@ApiResponse(code = 403, message = "Acesso bloqueado para este recurso!", response = ApiResponseMessage.class),
			@ApiResponse(code = 500, message = "Ocorreu um erro inesperado no servidor.", response = ApiResponseMessage.class) })
	public Response generateToken(@HeaderParam("Authorization") String authorization);
}
