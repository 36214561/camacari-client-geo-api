# README #

Implementação de cliente para acesso da API de Camaçari

### Código de exemplo de acesso aos recursos ###
```java
		try {
			
			String usuario = "teste";
			String senha = "123456s";
			
			Integer skipBairro = 1;
			Integer limitBairro = 50;
			String paramsBairro = "";
			
			BairroClient bairroCliente = new BairroClient(usuario, senha);
			
			CollectionVwBairro collBairro = bairroCliente.obterRecursos(paramsBairro, skipBairro, limitBairro);
			for (Iterator<VwBairro> i = collBairro.getItems().iterator(); i.hasNext();) {
				VwBairro bairro = i.next();
				System.out.println(bairro.getCdBairro() + " " + bairro.getNmBairro());
			}
		}catch (AplicacaoException e) {
			for (DescricaoValue descricaoValue : e.getErros()) {
				System.err.println(descricaoValue.getMensagem());
				System.err.println(descricaoValue.getDetalhe());
			}
		}catch (SystemException e) {
			e.printStackTrace();
		}
```